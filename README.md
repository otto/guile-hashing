
This repository consists of modified code of:  
[https://gitlab.com/weinholt/hashing/](https://gitlab.com/weinholt/hashing/)

The original is copyrighted to Göran Weinholt <goran@weinholt.se>

Modifications were made by Otto Jung <otto.jung@kakao.com>
